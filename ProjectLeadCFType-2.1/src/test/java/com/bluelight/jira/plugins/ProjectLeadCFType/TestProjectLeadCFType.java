package com.bluelight.jira.plugins.ProjectLeadCFType;

import com.atlassian.jira.config.properties.*;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.customfields.impl.UserCFType;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.customfields.converters.*;
import com.atlassian.jira.issue.customfields.manager.*;
import com.atlassian.jira.issue.customfields.persistence.CustomFieldValuePersister;
import com.atlassian.jira.security.*;
import com.atlassian.jira.bc.user.search.*;
import com.atlassian.jira.issue.fields.layout.field.FieldLayoutItem;
import com.atlassian.crowd.embedded.api.User;
import java.util.Map;
import java.util.HashMap;
import com.atlassian.jira.project.Project;
import org.apache.commons.lang.StringUtils;


import org.jmock.Mock;
import org.jmock.MockObjectTestCase;
import org.jmock.core.Stub;
import webwork.action.ActionContext;
import com.atlassian.jira.util.EasyList;


public class TestProjectLeadCFType extends MockObjectTestCase {
    /**
     * constructor
     */
    private CustomFieldValuePersister customFieldValuePersister;
    private UserConverter userConverter;
    private GenericConfigManager genericConfigManager;
    private ApplicationProperties applicationProperties;
    private JiraAuthenticationContext authenticationContext;
    private UserPickerSearchService searchService;
    private Map velocityParametersFromParent;			       

    protected ProjectLeadCFType getProjectLeadCFType()
    {
	return new ProjectLeadCFType(customFieldValuePersister, userConverter, genericConfigManager, applicationProperties, authenticationContext, searchService)
	{
	    protected Map getVelocityParametersFromParent(Issue issue, CustomField field, FieldLayoutItem fieldLayoutItem)
	    {
		return velocityParametersFromParent;
	    
	    }
	};
    }

    public void testGetValueFromIssue() {
	Mock mockIssue;
	Mock mockProject;
	Mock mockCustomField;
	Mock mockUser;
	
	Issue issue;
	Project project;
	CustomField customField;
	User user;

	mockUser = new Mock(User.class);
	user = (User) mockUser.proxy();

	mockProject = new Mock(Project.class);
	mockProject.expects(atLeastOnce()).method("getLeadUser").withNoArguments().will(onConsecutiveCalls(new Stub[] {
		    returnValue(null), 
		    returnValue(user), 
		    returnValue(user)}));
	project = (Project) mockProject.proxy();


	mockCustomField = new Mock(CustomField.class);
	customField = (CustomField) mockCustomField.proxy();

	mockIssue = new Mock(Issue.class);
	mockIssue.expects(atLeastOnce()).method("getProjectObject").withNoArguments().will(onConsecutiveCalls(new Stub[] {
		    returnValue(null),
		    returnValue(project),
		    returnValue(project),
		    returnValue(project),
		    returnValue(project),
		    returnValue(project)}));
	issue = (Issue) mockIssue.proxy();
	
	ProjectLeadCFType projectLeadCFType = getProjectLeadCFType();
	
	assertEquals(StringUtils.EMPTY, projectLeadCFType.getValueFromIssue(customField, issue));
 	assertEquals(StringUtils.EMPTY, projectLeadCFType.getValueFromIssue(customField, issue));
	assertEquals(user, projectLeadCFType.getValueFromIssue(customField, issue));
 	assertEquals(StringUtils.EMPTY, projectLeadCFType.getValueFromIssue(customField, null));
    }

    public void testgetVelocityParameters(){
	
	Mock mockIssue;
	Mock mockCustomField;
	Mock mockFieldLayoutItem;
	Mock mockProject;
	Mock mockUser;

	Issue issue;
	CustomField customField;
	User user;
	Project project;
 
	velocityParametersFromParent = new HashMap();
	velocityParametersFromParent.put("Parent Key", new Object());

	mockUser = new Mock(User.class);
	user = (User) mockUser.proxy();

	mockProject = new Mock(Project.class);
	mockProject.expects(atLeastOnce()).method("getLeadUser").withNoArguments().will(onConsecutiveCalls(new Stub[] {
		    returnValue(null), 
		    returnValue(user), 
		    returnValue(user)}));
	project = (Project) mockProject.proxy();


	mockCustomField = new Mock(CustomField.class);
	customField = (CustomField) mockCustomField.proxy();

	mockIssue = new Mock(Issue.class);
	mockIssue.expects(atLeastOnce()).method("getProjectObject").withNoArguments().will(onConsecutiveCalls(new Stub[] {
		    returnValue(null),
		    returnValue(project),
		    returnValue(project),
		    returnValue(project),
		    returnValue(project),
		    returnValue(project)}));
	issue = (Issue) mockIssue.proxy();

	// Test if getProjectObject() is null
	
	ProjectLeadCFType projectLeadCFType = getProjectLeadCFType();
	Map params = projectLeadCFType.getVelocityParameters(issue, customField, null);

	assertTrue(params.containsKey("Parent Key"));
	assertTrue(params.containsKey("value"));
	assertEquals("",params.get("value"));

	// Test if getLeadUser() is null

	projectLeadCFType = getProjectLeadCFType();
	params = projectLeadCFType.getVelocityParameters(issue, customField, null);

	assertTrue(params.containsKey("Parent Key"));
	assertTrue(params.containsKey("value"));
	assertEquals("",params.get("value"));

	// Test if everythink ok

	projectLeadCFType = getProjectLeadCFType();
	params = projectLeadCFType.getVelocityParameters(issue, customField, null);
	
	assertTrue(params.containsKey("Parent Key"));
	assertTrue(params.containsKey("value"));
	assertEquals(user,params.get("value"));

	// Test if issue is null and velocitParametersFromParent

	velocityParametersFromParent = null;
	projectLeadCFType = getProjectLeadCFType();
	params = projectLeadCFType.getVelocityParameters(null, customField, null);

	assertFalse(params.containsKey("Parent Key"));
	assertTrue(params.containsKey("value"));
	assertEquals("",params.get("value"));


    }



}








