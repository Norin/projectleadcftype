package com.bluelight.jira.plugins.ProjectLeadCFType;

import com.atlassian.jira.config.properties.*;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.customfields.impl.UserCFType;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.customfields.converters.*;
import com.atlassian.jira.issue.customfields.manager.*;
import com.atlassian.jira.issue.customfields.persistence.CustomFieldValuePersister;
import com.atlassian.jira.security.*;
import com.atlassian.jira.bc.user.search.*;
import com.atlassian.jira.issue.fields.layout.field.FieldLayoutItem;
import com.opensymphony.user.User;
import java.util.Map;
import java.util.HashMap;

public class ProjectLeadCFType extends UserCFType {
    /**
     * constructor
     */

  public ProjectLeadCFType(CustomFieldValuePersister customFieldValuePersister,
			   UserConverter userConverter, 
			   GenericConfigManager genericConfigManager, 
			   ApplicationProperties applicationProperties, 
			   JiraAuthenticationContext authenticationContext, 
			   UserPickerSearchService searchService
			   ) {
	super(customFieldValuePersister, 
	      userConverter, 
	      genericConfigManager, 
	      applicationProperties, 
	      authenticationContext, 
	      searchService);
  }

    // Get the ProjectLead form the Issue

    public Object getValueFromIssue(CustomField field, Issue issue) 
    {
	// make sure all arguments are not null
	if ( issue == null || issue.getProjectObject() == null  || issue.getProjectObject().getLeadUser() == null ) 
	    {
		return "";
	    }
	else 
	    {
		return issue.getProjectObject().getLeadUser();
	    }
    }

    protected Map<String, Object> getVelocityParametersFromParent(Issue issue, CustomField field, FieldLayoutItem fieldLayoutItem)
    {
	return super.getVelocityParameters(issue, field, fieldLayoutItem);
    }


    // To get the ProjectLead during the Create-Issue Screen and the Update-Screen right displayed
    
    public Map<String, Object> getVelocityParameters(Issue issue, CustomField field, FieldLayoutItem fieldLayoutItem)
    { 

	// get the Parameters via the Parent class method
	Map params = getVelocityParametersFromParent(issue,field,fieldLayoutItem);

	if ( params == null ) 
	    {
		params = new HashMap();
	    }

	if (issue == null || issue.getProjectObject() == null || issue.getProjectObject().getLeadUser() == null ) 
	    {
		params.put("value","");
	    } 
	else 
	    {
		// add the LeadUser to the parameters, which is read by the edit-velocity methods
		params.put("value",issue.getProjectObject().getLeadUser());
	    }

	return params;
    }
}








