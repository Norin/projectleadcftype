package com.bluelight.jira.plugins.ProjectLeadCFType;

import com.atlassian.jira.bc.user.search.*;
import com.atlassian.jira.issue.customfields.converters.*;
import com.atlassian.jira.issue.customfields.searchers.*;
import com.atlassian.jira.issue.customfields.searchers.transformer.*;
import com.atlassian.jira.jql.operand.*;
import com.atlassian.jira.jql.resolver.*;
import com.atlassian.jira.security.*;
import com.atlassian.jira.user.util.*;

/**
 * A custom searcher class that simply reuses an existing searcher.
 */
public class ProjectLeadUserPickerSearcher extends UserPickerSearcher {
    /**
     *
     * Default constructor. We simply forward the arguments to super.
     *
     * @param userResolver
     * @param operandResolver
     * @param context
     * @param userConverter
     * @param userPickerSearchService
     * @param customFieldInputHelper
     */
    public ProjectLeadUserPickerSearcher(UserResolver userResolver,
		       JqlOperandResolver operandResolver,
		       JiraAuthenticationContext context,
		       UserConverter userConverter,
		       UserPickerSearchService userPickerSearchService,
		       CustomFieldInputHelper customFieldInputHelper,
					 UserManager userManager 	) {
	super(userResolver,
	      operandResolver,
	      context,
	      userConverter,
	      userPickerSearchService,
	      customFieldInputHelper,
			userManager);
    }
}
